
// Created by bl on 3/23/19.
//
#include "vtkPointCloud.h"

#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>

#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/crop_box.h>

#include <vtkProperty.h>
#include <vtkPointData.h>
#include <vtkActor.h>




vtkPointCloud::vtkPointCloud(std::string path) {

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cropped(new pcl::PointCloud<pcl::PointXYZRGB>);
    //pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZRGB>);

    if (pcl::io::loadPCDFile<pcl::PointXYZRGB>(path, *cloud) == -1){ //* load the file
        PCL_ERROR ("Couldn't read file test_pcd.pcd \n");
    }
    //pcl::PCDWriter w;
    //w.writeBinary("/home/bjba/.bjba/dataset/test001/" + path.substr(path.length()-8,8),*cloud);


    Eigen::Affine3f t = Eigen::Affine3f::Identity();
    t.translate(Eigen::Vector3f(cloud->sensor_origin_[0], cloud->sensor_origin_[1], cloud->sensor_origin_[2]));
    t.rotate(cloud->sensor_orientation_);
    t.inverse();

    pcl::CropBox<pcl::PointXYZRGB> boxFilter;
    boxFilter.setInputCloud(cloud);
    boxFilter.setMin({-0.80f,-0.11f,-0.02f,1.0f});
    boxFilter.setMax({-0.53f, 0.07f, 0.20f,1.0f});
    boxFilter.setTransform(t);
    boxFilter.filter(*cloud_cropped);

    /*pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
    sor.setInputCloud (cloud_cropped);
    sor.setMeanK (50);
    sor.setStddevMulThresh (2.0);
    sor.filter (*cloud_filtered);*/

    origin = vtkSmartPointer<vtkTransform>::New();
    origin->Translate(cloud->sensor_origin_[0], cloud->sensor_origin_[1], cloud->sensor_origin_[2]);
    Eigen::AngleAxisf aa(cloud->sensor_orientation_);
    origin->RotateWXYZ(aa.angle()*57.2957795, aa.axis()[0],aa.axis()[1], aa.axis()[2]);

    points = vtkSmartPointer<vtkPoints>::New();
    colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
    colors->SetNumberOfComponents(4);
    colors->SetName ("Colors");

    for (auto &point : cloud_cropped->points) {
        unsigned char c[4] = {point.r, point.g, point.b, 128};
        points->InsertNextPoint(point.x, point.y, point.z);
        colors->InsertNextTypedTuple(c);
    }

    pointsPolydata = vtkSmartPointer<vtkPolyData>::New();
    pointsPolydata->SetPoints(points);

    transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    transformFilter->SetInputData(pointsPolydata);
    transformFilter->SetTransform(origin);
    transformFilter->Update();

    vertexFilter = vtkSmartPointer<vtkVertexGlyphFilter>::New();

    vertexFilter->SetInputData(transformFilter->GetOutput());
    vertexFilter->Update();

    polydata = vtkSmartPointer<vtkPolyData>::New();
    polydata->ShallowCopy(vertexFilter->GetOutput());

    polydata->GetPointData()->SetScalars(colors);

    mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputData(polydata);

    actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->SetPointSize(1);
}