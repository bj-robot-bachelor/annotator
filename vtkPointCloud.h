//
// Created by bl on 3/23/19.
//

#ifndef BJBA_VTKPOINTCLOUD_H
#define BJBA_VTKPOINTCLOUD_H

#include <string>
#include <vtkPoints.h>
#include <vtkUnsignedCharArray.h>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkTransform.h>
#include <vtkPolyDataMapper.h>
#include <vtkTransformPolyDataFilter.h>

class vtkPointCloud {
public:
    vtkPointCloud(std::string);

    vtkSmartPointer<vtkPoints> points;
    vtkSmartPointer<vtkUnsignedCharArray> colors;
    vtkSmartPointer<vtkPolyData> pointsPolydata;
    vtkSmartPointer<vtkVertexGlyphFilter> vertexFilter;
    vtkSmartPointer<vtkPolyData> polydata;
    vtkSmartPointer<vtkTransform> origin;
    vtkSmartPointer<vtkPolyDataMapper> mapper;
    vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter;
    vtkSmartPointer<vtkActor> actor;

};


#endif //BJBA_VTKPOINTCLOUD_H

//