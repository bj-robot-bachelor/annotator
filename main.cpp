/*=========================================================================

  Program:   Visualization Toolkit
  Module:    Cone5.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
//
// This example introduces the concepts of interaction into the
// C++ environment. A different interaction style (than
// the default) is defined.
//

// First include the required header files for the VTK classes we are using.
#include <string>
#include <vector>

#include <vtkInteractorStyleJoystickCamera.h>
#include <vtkInteractorStyleUnicam.h>
#include <vtkInteractorStyleTerrain.h>
#include <vtkInteractorStyleFlight.h>
#include <vtkParallelCoordinatesInteractorStyle.h>
#include <vtkInteractorStyleTrackball.h>
#include <vtkSTLReader.h>

#include "vtkConeSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkCamera.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include <vtkProperty.h>
#include <vtkPointData.h>
#include <vtkUnsignedCharArray.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkAxesActor.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>

#include "vtkPointCloud.h"

#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

int main()
{
    std::string inputFilename = "AngleTubeSmall.stl";

    vtkSmartPointer<vtkSTLReader> reader =
            vtkSmartPointer<vtkSTLReader>::New();
    reader->SetFileName(inputFilename.c_str());
    reader->Update();

    // Visualize
    vtkSmartPointer<vtkPolyDataMapper> mapper =
            vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(reader->GetOutputPort());

    vtkSmartPointer<vtkActor> model_actor =
            vtkSmartPointer<vtkActor>::New();
    model_actor->SetMapper(mapper);

    std::vector<vtkPointCloud> clouds;

    std::string path = "/home/bjba/.bjba/dataset/test002/";

    int c = 0;

    for (const auto & entry : fs::directory_iterator(path)){
     if(entry.path().extension() == ".pcd") {
            clouds.emplace_back(entry.path().string());
            cout << ++c << endl;
        }
    }


    vtkSmartPointer<vtkAxesActor> axes = vtkSmartPointer<vtkAxesActor>::New();

    axes->SetTotalLength(.1,.1,.1);
    axes->SetAxisLabels(false);


    /** cloud render setup**/
    vtkSmartPointer<vtkRenderer> cloud_renderer = vtkSmartPointer<vtkRenderer>::New();

    for (auto &cloud : clouds) {
        cloud_renderer->AddActor(cloud.actor);
    }

    //renderer->AddActor(axes);
    cloud_renderer->SetBackground(.1,.2,.3);
    cloud_renderer->SetViewport(0,0,.5,1);

    /** model render setup**/
    vtkSmartPointer<vtkRenderer> model_renderer = vtkSmartPointer<vtkRenderer>::New();

    model_renderer->AddActor(model_actor);
    model_renderer->SetBackground(.17,.24,.31);
    model_renderer->SetViewport(0.5,0,1,1);


    vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();

    renderWindow->AddRenderer(cloud_renderer);
    renderWindow->AddRenderer(model_renderer);

    renderWindow->SetSize(1200,600);

    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    vtkInteractorStyleTrackballCamera *style = vtkInteractorStyleTrackballCamera::New();
    renderWindowInteractor->SetInteractorStyle(style);
    renderWindowInteractor->SetRenderWindow(renderWindow);

    renderWindow->Render();
    renderWindowInteractor->Start();


    return 0;
}